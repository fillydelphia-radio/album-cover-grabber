const http = require('http');
const express = require('express');
const app = express();
const axios = require('axios')
const sharp = require('sharp');
const fs = require('fs');
var mm = require('musicmetadata');

const port = 9990;

app.get('/:size?', (req, res) => {

    if(parseInt(req.params.size) > 750) {
        res.status(404)
            .send('{"status": "error", "message": "Invalid dimension"}')
        
    } else (

        axios.get('https://fillydelphia-radio.firebaseio.com/rest/fillydelphia-radio/now-playing/filename.json')
            .then(function (response) {
                
                var image;
                var mimetype;

                console.dir(response.data);

                if (parseInt(req.params.size) === 0 || !req.params.size || isNaN(req.params.size)) {
                    req.params.size = "500"
                }

                if(response.data === undefined) {
                    image.on('error', function() {
                        log.fatal('You didn\'t put an acceptable cover in the coveroutput folder!!!')
                    })
                    res.type('image/png')
                    sharp('./coveroutput/nocover.png').resize(parseInt(req.params.size)).png().pipe(res);
                } else {
                    // id3({ file: response.data, type: id3.OPEN_LOCAL }, function(err,tags) {
                    //     console.log(tags)
                    //     image = tags.v2.image.data
                    //     mimetype = tags.v2.image.mime
                    // })

                    var filePath = response.data.replace(/&#39;/g, "'")

                    var readableStream = fs.createReadStream(filePath);
                    var parser = mm(readableStream, function (err, metadata){
                        if (!metadata.picture || metadata.picture === 'undefined' ||metadata.picture.length === 0) {
                            console.log(JSON.stringify(metadata))
                            res.type('image/png')
                            sharp('./coveroutput/nocover.png').resize(parseInt(req.params.size)).png().pipe(res);
                            
                        } else {
                            console.log(metadata)
                            image = metadata.picture[0].data
                            res.type('image/png')
                            sharp(image).resize(parseInt(req.params.size)).png().pipe(res);
                        }
                    })
                }
                
            } 
        )
    )
})


app.listen(port, (err) => {
    if (err) {
        return console.log('99 something bad happened', err)
    }

    console.log(`102 server is listening on ${port}`)
})